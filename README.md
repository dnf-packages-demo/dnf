# dnf

Package manager based on libdnf and libsolv. https://github.com/rpm-software-management/dnf

# `dnf5` replaces `microdnf`
* [*Changes/MajorUpgradeOfMicrodnf*
  ](https://fedoraproject.org/wiki/Changes/MajorUpgradeOfMicrodnf#Release_Notes)
* [rpm-software-management/microdnf](https://github.com/rpm-software-management/microdnf)
  * But microdnf is still been updated!
  * But version numbers are different.

# Official documentation
* [*Upgrading Fedora Linux Using DNF System Plugin*
  ](https://docs.fedoraproject.org/en-US/quick-docs/upgrading-fedora-offline/)
  (2024-04) Michael Wu, Anthony McGlone, The Fedora Docs team Version F38, F39, F40

# Unofficial documentation
* [*How To Speed Up DNF Package Manager In Fedora, RHEL, CentOS, AlmaLinux, Rocky Linux*
  ](https://ostechnix.com/how-to-speed-up-dnf-package-manager-in-fedora/)
  2021-11 Senthilkumar Palani (aka SK)
